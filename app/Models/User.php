<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Notifications\ResetPassword;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

use App\Http\Traits\BaseModelTrait;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, BaseModelTrait, HasRelationships;

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\Models\User
     */
    public function findForPassport($username)
    {
        return $this->where('username', $username)->first();
    }

    /**
     * OVERWRITTEN ELOQUENT MODEL PROPERTIES
     */

    /**
     * Table name
     * @var string
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'username',
        'firstname',
        'lastname',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types
     * @var array
     */
    protected $casts = [
        //
    ];

    /**
     * Additional attributes
     * @var array
     */
    protected $appends = [
        'last_login'
    ];

    /**
     * CUSTOM PROPERTIES
     */

    /**
     * List of relations
     */
    protected $relationNames = [
        //
    ];

     /**
     * Attribute / relation names to change when return JSON response (via toArray())
     */
    protected $attributeNamesMap = [
        //
    ];

    /**
     * RELATIONS
     */

    // ...

    /**
     * SCOPES
     */

    // ...

    /**
     * ADDITIONAL ATTRIBUTE GETTERS
     */

    /**
     * 'last_login' attribute
     */
    public function getLastLoginAttribute()
    {
        return $this->lastLogin();
    }

    /**
     * CUSTOM METHODS
     */

    /**
     * Return last user login date (based on last token)
     * @return string
     */
    public function lastLogin () {
        $lastToken = DB::table('oauth_access_tokens')->where('user_id', $this->id)->latest()->first();
        return $lastToken ? $lastToken->created_at : null;
    }

    /**
     * Return true if the user is admin
     * @return bool
     */
    public function isAdmin(){
        if($this->role == "admin"){
            return true;
        }
        return false;
    }

    /**
     * Validate password
     * @param string $password - The password to check
     * @return string|bool - If password is valid, returns crypted password, else returns false
     */
    public static function validatePassword($password)
    {
        $passwordPolicy = new PasswordPolicy();
        return $passwordPolicy->passes('password', $password);
    }

    /**
     * OVERWRITTEN BASE MODEL METHODS
     */

    /**
     * Define data visibility depending of user rights
     * @param Array $data - The original data
     * @return array
     */
    public static function dataVisibility ($data) {
        // Create base array with all data attributes to 'true'
        $visibility = array_fill_keys(array_keys($data), true);
        // Do logic here
        return $visibility;
    }

    /**
     * OVERWRITTEN ELOQUENT MODEL METHODS
     */

    /**
     * Overwrite toArray() method
     * @return array
     */
    public function toArray()
    {
        // Attributes
        $attributes = $this->attributesToArray();
        // Get related resources array
        $related = $this->relationsToArray();
        // Full data
        $data = array_merge($attributes, $related);
        // Do logic here...
        // Return formatted & cleaned object
        return static::cleanData($data);
    }

}
