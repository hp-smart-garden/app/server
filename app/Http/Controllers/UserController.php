<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Auth\Events\Registered;
use \Image;

use App\Models\Error;
use App\Models\User;

use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;

class UserController extends Controller
{
    /**
     * Create a new controller instance
     * @codeCoverageIgnore
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @OA\Post(
     *     path="/users/create",
     *     operationId="users.create",
     *     summary="Create user",
     *     description="**Créer un nouvel utilisateur**",
     *     tags={"Users"},
     *     @OA\RequestBody(
     *         description="User data to create",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.User.Create")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     *
     * Create new user
     * @param CreateRequest $request - The request
     * @return User|Response
     */
    public function create (CreateRequest $request)
    {
        // Validate the request
        $validated = $request->validated();
        // New user
        $newUser = new User();
        // Username
        $newUser->username = $validated['username'];
        // Password
        $newUser->password = Hash::make($validated['password']);
        // Save
        $newUser->save();
        // Emit 'registered' event
        event(new Registered($newUser));
        // Return created user
        return $newUser;
    }

    /**
     * @OA\Patch(
     *     path="/users/{userId}",
     *     operationId="users.update",
     *     summary="Update user",
     *     description="**Modifier l'utilisateur courant**  
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Users"},
     *     @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="Identifiant de l'utilisateur (ID)",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         description="User data to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.User.Update")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     *
     * Update existing user
     * @param UpdateRequest $request - The request
     * @param User $user - The request
     * @return User|Response
     */
    public function update (UpdateRequest $request, User $user)
    {
        // Validate the request
        $validated = $request->validated();
        // Password security
        $doesPasswordMatches = Hash::check($validated['password'], $user->password);
        if (!$doesPasswordMatches) {
            return errorResponse(401, Error::find('e0022'));
        }
        // Assign each new property value
        foreach ($validated as $key => $value) {
            // Ignore password
            if ($key !== 'password') {
                $user->$key = $value;
            }
        }
        // Save and return modified user
        $user->save();
        return $user;
    }

}
