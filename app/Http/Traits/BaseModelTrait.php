<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

use App\Support\BaseCollection;

trait BaseModelTrait {

    /**
     * OVERWRITTEN ELOQUENT MODEL METHODS
     */

    /**
     * Create a new Eloquent Collection instance
     * @param array $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new BaseCollection($models);
    }

    /**
     * SCOPES
     */

    /**
     * Scope a query to include all relations
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithRelations($query)
    {
        $relations = array_map(function ($name) {
            return Str::camel($name);
        }, $this->relationNames);
        return $query->with($relations);
    }

    /**
     * METHODS
     */
    
    /**
     * Get columns of the model (static method)
     * @return array
     */
    public static function getColumnNames () {
        return Schema::getColumnListing(static::getTableName());
    }
    
    /**
     * Get table of a model (static method)
     * @return string
     */
    public static function getTableName () {
        $temp_instance = new static();
        return $temp_instance->getTable();
    }

    /**
     * Get relations of the model (static method)
     * @return array
     */
    public static function getModelRelations () {
        $temp_instance = new static();
        return $temp_instance->relationNames;
    }

    /**
     * Get hidden attributes of the model (static method)
     * @return array
     */
    public static function getHiddenAttributes () {
        $temp_instance = new static();
        return $temp_instance->getHidden();
    }

    /**
     * Define data visibility depending of user rights
     * @param array $data - The original data
     * @return array
     */
    public static function dataVisibility (array $data) {
        // Create base array with all data attributes to 'true'
        $visibility = array_fill_keys(array_keys($data), true);
        // Do logic here
        return $visibility;
    }

    /**
     * Clean object data based on user permissions
     * @param array $data - Object data to clean
     * @return array
     */
    public static function cleanData (array $data) {
        // Prepare empty array to return
        $cleaned = [];
        // Create temporary instance
        $temp_instance = new static();
        // Clean data based on data visibility method return
        $visibility = static::dataVisibility($data);
        foreach ($visibility as $attr => $visible) {
            // If visible and attr exists
            if ($visible) {
                // Get attribute name
                $attributeName = isset($temp_instance->attributeNamesMap[$attr])
                    ? $temp_instance->attributeNamesMap[$attr]
                    : $attr;
                // Assign value
                $cleaned[$attributeName] = $data[$attr];
            }
        }
        return $cleaned;
    }

    /**
     * Return the instance without relations (similar to toArray() method, returning only model attributes)
     * @return array
     */
    public function withoutRelations()
    {
        // Attributes
        $data = $this->attributesToArray();
        // Return formatted & cleaned object
        return static::cleanData($data);
    }

}
