<?php

namespace App\Http\Traits;

trait ExcludableTrait {
    /**
     * Exclude some field from the returned item
     */
    public function scopeExclude($query, $value = []) 
    {
        $table = $this->getTable();
        $columns = Schema::getColumnListing($table);
        return $query->select(array_diff($columns, (array) $value));
    }
}
