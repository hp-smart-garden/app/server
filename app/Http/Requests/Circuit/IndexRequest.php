<?php

namespace App\Http\Requests\Circuit;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;
use App\Models\Circuit;

/**
 * @OA\Schema(schema="Requests.Circuit.Index")
 */
class IndexRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\CircuitPolicy::index
        Gate::authorize('index', Circuit::class);
        return true;
    }

    /**
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
