<?php

namespace App\Http\Requests\Circuit;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;
use App\Rules\HexColorFormat;

/**
 * @OA\Schema(schema="Requests.Circuit.Update")
 */
class UpdateRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\CircuitPolicy::update
        Gate::authorize('update', $this->route('circuit'));
        return true;
    }

    /**
     * @OA\Property(
     *     property="channel",
     *     type="integer",
     *     description="Canal du circuit"
     * ),
     * @OA\Property(
     *     property="name",
     *     type="string",
     *     description="Nom du circuit"
     * ),
     * @OA\Property(
     *     property="type",
     *     type="string",
     *     enum={"valve", "water_inlet"},
     *     description="Type de circuit ('valve' ou 'water_inlet')",
     *     default="valve"
     * ),
     * @OA\Property(
     *     property="description",
     *     type="string",
     *     description="Description optionnelle du circuit",
     *     nullable=true
     * ),
     * @OA\Property(
     *     property="color",
     *     type="string",
     *     description="Couleur représentative du circuit (format: '#XXXXXX', length: 7 chars)"
     * )
     *
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'channel' => [
                'integer',
                // Prevent same channel for a circuit
                Rule::unique('circuits')->where(function ($query) {
                    $query = $query->where('id', '!=', $this->route('circuit')->id);
                })
            ],
            'name' => 'string|max:255',
            'description' => 'nullable|string|max:255',
            'type' => [
                Rule::in(['valve', 'water_inlet'])
            ],
            'color' => [
                'string',
                'size:7', // Exactly 7 characters
                new HexColorFormat
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
