<?php

namespace App\Http\Requests\Auth;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;

/**
 * @OA\Schema(schema="Requests.Auth.Login") {
 *     required={
 *         "username",
 *         "password"
 *     }
 * }
 */
class LoginRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // PUBLIC ROUTE
        return true;
    }

    /**
     * @OA\Property(
     *     property="username",
     *     type="string",
     *     description="Nom d'utilisateur"
     * ),
     * @OA\Property(
     *     property="password",
     *     type="string",
     *     description="Mot de passe de l'utilisateur"
     * )
     * 
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string',
            'password' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
