<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Admin user
        User::create([
            'id' => 1,
            'password' => bcrypt('admin'),
            'username' => 'admin',
            'firstname' => 'John',
            'lastname' => 'Doe',
            'role' => 'admin'
        ]);

    }
}
