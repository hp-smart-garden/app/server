# --------------------------------------
# Create PHP Image
# See also: https://github.com/docker-library/docs/blob/master/php/README.md#supported-tags-and-respective-dockerfile-links
# --------------------------------------

# --------------------------------------
# Stage: base
# --------------------------------------
FROM php:7.3.21-fpm as base

# The maintainer name and email
LABEL maintainer="Hervé Perchec <herve.perchec@gmail.com>"

# Working directory
WORKDIR /var/www/html

# Copy xdebug config files
COPY .docker/usr/local/etc/php/conf.d/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY .docker/usr/local/etc/php/conf.d/error_reporting.ini /usr/local/etc/php/conf.d/error_reporting.ini

# Install composer
COPY --from=composer:2.0.13 /usr/bin/composer /usr/local/bin/composer

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq \
    git \
    curl \
    libmcrypt-dev \
    libjpeg-dev \
    libpng-dev \
    libfreetype6-dev \
    libbz2-dev \
    zlib1g-dev \
    zip \
    unzip \
    libzip-dev \
    netcat

# Install xdebug
RUN pecl install xdebug

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Configure PHP extensions
RUN docker-php-ext-configure gd \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/\
    --with-freetype-dir=/usr/include/

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install \
    zip \
    pdo \
    pdo_mysql \
    gd

# Enable PHP extensions
RUN docker-php-ext-enable xdebug

# Copy entrypoint to /usr/local/bin to call it as a command
COPY .docker/docker-entrypoint.sh /usr/local/bin/
# Symbolic link
RUN ln -s usr/local/bin/docker-entrypoint.sh /entrypoint.sh # backwards compat

# Call entrypoint
ENTRYPOINT ["docker-entrypoint.sh"]

# --------------------------------------
# Stage: dev
# --------------------------------------
FROM base as dev

# Install openssh-server and nano
RUN apt-get -y --no-install-recommends install --fix-missing \
    openssh-server \
    nano
# Clear a list of repos to make our docker image smaller
RUN rm -rf /var/lib/apt/lists/*
# Copy sshd_config
COPY .docker/etc/ssh/sshd_config /etc/ssh/sshd_config
# Copy etc/pam.d/common-auth to enable root login without password
COPY .docker/etc/pam.d/common-auth /etc/pam.d/common-auth
# Set NO PASSWORD for root user
RUN passwd -d root
# Copy custom ~/.bashrc file
COPY .docker/root/.bashrc /root/.bashrc
# Change default shell to /bin/bash
RUN chsh -s /bin/bash

# --------------------------------------
# Stage: prod
# --------------------------------------
FROM base as prod