<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Laravel\Passport\Passport;

use App\Models\Circuit;

class CircuitTest extends TestCase
{
    /**
     * Set up method
     */
    public function setUp() :void
    {
        // Always call parent set up
        parent::setUp();
        // Do logic here
    }

    /**
     * Test circuits index
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::index
     * @covers App\Policies\CircuitPolicy::index
     * @covers App\Http\Requests\Circuit\IndexRequest
     * 
     * @return Circuit[]
     */
    public function test_circuits_index ()
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->get('/api/circuits');
        $response->assertStatus(200);
    }

    /**
     * Test create circuit
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::create
     * @covers App\Policies\CircuitPolicy::create
     * @covers App\Http\Requests\Circuit\CreateRequest
     * 
     * @return Circuit
     */
    public function test_create_circuit ()
    {
        // Valid request
        $response = $this->post('/api/circuits/create', [
            'channel' => 4,
            'name' => 'new circuit',
            'description' => 'New circuit awesome description',
            'color' => '#FF0022'
        ]);
        $response->assertStatus(201);
        // Get created circuit ID
        $circuitId = json_decode($response->content())->id;
        // Return created circuit
        return Circuit::findOrFail($circuitId);
    }

    /**
     * Test retrieve circuit
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::retrieve
     * @covers App\Policies\CircuitPolicy::retrieve
     * @covers App\Http\Requests\Circuit\RetrieveRequest
     * 
     * @depends test_create_circuit
     * 
     * @param Circuit $circuit - The previously created circuit
     * @return void
     */
    public function test_retrieve_circuit (Circuit $circuit)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->get('/api/circuits/create');
        $response->assertStatus(200);
        // Get created circuit ID
        $circuitId = json_decode($response->content())->id;
    }

    /**
     * Test update circuit
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::update
     * @covers App\Policies\CircuitPolicy::update
     * @covers App\Http\Requests\Circuit\UpdateRequest
     * 
     * @depends test_create_circuit
     * 
     * @param Circuit $circuit - The previously created circuit
     * @return void
     */
    public function test_update_circuit (Circuit $circuit)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->patch('/api/circuits/' . $circuit->id, [
            'channel' => 6,
            'name' => 'updated name',
            'description' => 'updated description',
            'color' => '#FF5588'
        ]);
        $response->assertStatus(200);
    }

    /**
     * Test delete circuit
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::delete
     * @covers App\Policies\CircuitPolicy::delete
     * @covers App\Http\Requests\Circuit\DeleteRequest
     * 
     * @depends test_delete_circuit
     * 
     * @param Circuit $circuit - The previously created circuit
     * @return void
     */
    public function test_delete_circuit (Circuit $circuit)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->delete('/api/circuits/' . $circuit->id);
        $response->assertStatus(200);
    }

    /**
     * Test get circuit status
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::status
     * @covers App\Policies\CircuitPolicy::status
     * @covers App\Http\Requests\Circuit\StatusRequest
     * 
     * @return void
     */
    public function test_circuit_status ()
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->get('/api/circuits/1/status'); // Get status of circuit id:1
        $response->assertStatus(200);
    }

    /**
     * Test turn circuit on/off
     * 
     * @author Hervé Perchec
     * @group Circuit
     * @covers App\Http\Controllers\CircuitController::turn
     * @covers App\Policies\CircuitPolicy::turn
     * @covers App\Http\Requests\Circuit\TurnRequest
     * 
     * @return void
     */
    public function test_turn_circuit ()
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->get('/api/circuits/1/turn', [
            'status' => 'on' // Get turn on circuit 1
        ]);
        $response->assertStatus(200);
    }

}
