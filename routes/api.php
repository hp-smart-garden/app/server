<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CircuitController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * -------------
 * PUBLIC ROUTES
 * -------------
 */

/**
 * Users
 */

/**
 * [POST] /api/login
 * 
 * Tested in: - tests/Feature/AuthTest::test_login
 */
Route::post('/login', [AuthController::class, 'login'])
     ->name('auth.login');

/**
 * ---------------------
 * PRIVATE (AUTH) ROUTES
 * ---------------------
 */

Route::group( [ 'middleware' => [ 'auth:api' ] ] , function () use ($router) {
    
    /**
     * Auth
     */

    /**
     * [GET] /api/me
     * 
     * Tested in: - tests/Feature/AuthTest::test_get_user_data
     */
    $router->get('/me', [AuthController::class, 'getCurrentUser'])
           ->name('auth.me');
    
    /**
     * [PATCH] /api/me/password
     * 
     * Tested in: - tests/Feature/AuthTest::test_update_user_password
     */
    $router->patch('/me/password', [AuthController::class, 'updatePassword']) // Specific for password
           ->name('password.update');
    
    /**
     * [POST] /api/logout
     * 
     * Tested in: - tests/Feature/AuthTest::test_logout
     */
    $router->post('/logout', [AuthController::class, 'logout'])
           ->name('auth.logout');

    /**
     * Circuits
     */

    /**
     * [GET] /api/circuits
     * 
     * Tested in: - tests/Feature/CircuitTest::test_circuits_index
     */
    $router->get('/circuits', [CircuitController::class, 'index'])
           ->name('circuits.index');

    /**
     * [POST] /api/circuits/create
     * 
     * Tested in: - tests/Feature/CircuitTest::test_create_circuit
     */
    $router->post('/circuits/create', [CircuitController::class, 'create'])
           ->name('circuits.create');

    /**
     * [GET] /api/circuits/{circuitId}
     * 
     * Tested in: - tests/Feature/CircuitTest::test_retrieve_circuit
     */
    $router->get('/circuits/{circuit}', [CircuitController::class, 'retrieve'])
           ->where('circuit', '[0-9]+')
           ->name('circuits.retrieve');

    /**
     * [PATCH] /api/circuits/{circuitId}
     * 
     * Tested in: - tests/Feature/CircuitTest::test_update_circuit
     */
    $router->patch('/circuits/{circuit}', [CircuitController::class, 'update'])
           ->where('circuit', '[0-9]+')
           ->name('circuits.update');

    /**
     * [DELETE] /api/circuits/{circuitId}
     * 
     * Tested in: - tests/Feature/CircuitTest::test_delete_circuit
     */
    $router->delete('/circuits/{circuit}', [CircuitController::class, 'delete'])
           ->where('circuit', '[0-9]+')
           ->name('circuits.delete');

    /**
     * [GET] /api/circuits/{circuitId}/status
     * 
     * Tested in: - tests/Feature/CircuitTest::test_circuit_status
     */
    $router->get('/circuits/{circuit}/status', [CircuitController::class, 'status'])
           ->where('circuit', '[0-9]+')
           ->name('circuit.status');

    /**
     * [POST] /api/circuits/{circuitId}/turn
     * 
     * Tested in: - tests/Feature/CircuitTest::test_turn_circuit
     */
    $router->post('/circuits/{circuit}/turn', [CircuitController::class, 'turn'])
           ->where('circuit', '[0-9]+')
           ->name('circuit.turn');

});
