# 🍃 *Smart Garden App* (Server) 🌐

[![smart-garden](https://img.shields.io/static/v1?label=&message=Smart%20Garden&color=191919&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACkElEQVQ4T3VTTUhUURQ+Z37fjFpMmFmr3LZuU4sKyqKgRWZ/IES1MIICNRlTsgchpowWA0ltLFKohqJ9EGVB1KZNFAVqfxIWVI5vft57c+89nfucNw1iFw7vncv5vvOdn4vwnzNG73cRFEcFWMoGqyuJ+5+sFIrLL+/QfJMLdqoEVgsb+CYg90hA4byJZ2arMRWCDFGtA9YFAfnOEiwaPtCFRWAwE+UAyHEiUl4LhNyBJCYtTeQR3CM6qkCMMHADWzmr/9UqciChsBBSIm1I1R+Tcp6t+2RtzyTeJAqvArI5IKCzLRH44NwPVvSCCZ4TZO+b2P0z7YzMGlI2xaVw2+p6o5gmitZDyfaBZcnPJNi9HbDzFSBSdc037OGMIdShuJR0eHVfALn2CNfu+ASccSoE2NyOm0s+0CQzbqJZ0P54fuh2VMrjTAAHEhcRb9EnA6Gm6MsWUNxxDrdO6WCTrjcCqE38+8HEs9/13UTuypQhxDZNsG/NpSUFFsxVKbCbHPimXFCnFNhvTTj9kFvtlTGRH1gflPiFweG4kNBcD0GPYAGmywSWHtk4N+2PA9l+E9s92d4hwMn84F1DyCM6uyaghkCYCd5FfoNy3PLScA+GktjSU924sYXBRDwSTEdLpTYPXLaslTXQJAqthTdaQaAEhccK5lsdEK+JnK+GUjM8snVsuxlU52fmEUKUSH399THmLVKaXh5zIZuSkJuxYXGa38CJkJLAQOCF8czPWqN9JecQsWtLw+VMZZVNytSGwe3hHnQiuTFeWQ5U/8BCaL/I4JQjC0N7GlP5yipX12vS1Y0RwmFDUiuPCyvZlXwQAtW9N2F+XvExLX+V6eLw9hjBKBMEY0J1HEz0PV0eo/2/bjSnwFQoTgQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/smart-garden)
[![app-server](https://img.shields.io/static/v1?labelColor=191919&label=server&message=v1.0.0&color=191919&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8S////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8S////v////9/////f////3////9/////f////3////9/////f////3////9/////f////3////9/////f////v//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////+////j////4////+P////j/////7//////////////////////////////////////////////////////////v///4////+P////j////4/////+///////////////////////////////////////////////////////////////////////////////////////////////A////4f///+D////g////4P///+D////g////4P///+D////g////4P///+D////g////4P///+H////A////D////x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////D////w////8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///w/////A////4f///+D////g////4P///+D////g////4P///+D////g////4P///+D////g////4P///+H////A//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7///+P////j////4////+P/////v/////////////////////////////////////////////////////////+////j////4////+P////j/////7//////////////////////////////////////////////////////////////////////////////////////////////7/////f////3////9/////f////3////9/////f////3////9/////f////3////9/////f////3////7////8S////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==)](https://gitlab.com/hperchec/smart-garden/app/server)
[![scorpion](https://img.shields.io/static/v1?label=&message=Scorpion&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&labelColor=2496ED&message=Docker&color=2496ED)](https://docker.com/)
[![Laravel](https://shields.io/static/v1?logo=laravel&logoColor=white&label=&labelColor=FB7470&message=Laravel&color=FB7470)](https://laravel.com/)
[![pipeline status](https://gitlab.com/hperchec/smart-garden/app/server/badges/main/pipeline.svg)](https://gitlab.com/hperchec/smart-garden/app/server/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

> This project is based on [Scorpion](https://gitlab.com/hperchec/boilerplates/scorpion/app/server) boilerplate **server** project

**Table of contents**:

[[_TOC_]]

## Get started

> **IMPORTANT**: Please refer to the [project documentation](https://gitlab.com/hperchec/smart-garden/docs).

Clone this repository:

```bash
git clone https://gitlab.com/hperchec/smart-garden/app/server.git
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)